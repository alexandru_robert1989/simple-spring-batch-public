package com.example.simplespringbatch.config;

import com.example.simplespringbatch.batch.JobCompleteNotificationListener;
import com.example.simplespringbatch.batch.UserProcessor;
import com.example.simplespringbatch.batch.UserReader;
import com.example.simplespringbatch.batch.UserWriter;
import com.example.simplespringbatch.model.User;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing
@AllArgsConstructor
public class SpringBatchConfig {

    //   private final String pathResource = "src/main/resources/users.csv";
    private final StepBuilderFactory stepBuilderFactory;
    private final JobBuilderFactory jobBuilderFactory;

    /**
     * Create a spring batch {@link Job} object using a
     * {@link JobBuilderFactory}.
     * <p>
     *
     * @return a {@link Job}
     */
    @Bean
    public Job userJob(Step userStep, Step userStepCopy) {

        // The RunIdIncrementer adds 1 after each batch execution.
        // If you have only one step use start, if not use flow, next and end (end is mandatory).

        return jobBuilderFactory.get("ETL")
                .incrementer(new RunIdIncrementer())
                .listener(jobUserListener())
                .start(userStep)
                //   .flow(userStep)
                //  .next(userStepCopy)
                //    .end()
                .build();
    }

    @Bean
    public Step userStep() {

        //A Step has a reader, a processor and a writer
        // chunk represents the total items to treat for one step
        return stepBuilderFactory.get("ETL-file-load")
                .<User, User>chunk(100)
                .reader(userReaderD())
                .processor(userItemProcessor())
                .writer(userWriterProcessor())
                .build();
    }

    @Bean
    public Step userStepCopy() {
        //A Step has a reader, a processor and a writer
        // chunk represents the total items to treat for one step
        return stepBuilderFactory.get("ETL-file-load-copy")
                .<User, User>chunk(50)
                .reader(userReaderD())
                .processor(userItemProcessor())
                .writer(userWriterProcessor())
                .build();
    }


    @Bean
    public ItemProcessor<User, User> userItemProcessor() {
        return new UserProcessor();
    }

    @Bean
    public ItemWriter<User> userWriterProcessor() {
        return new UserWriter();
    }

    @Bean
    public JobCompleteNotificationListener jobUserListener() {
        return new JobCompleteNotificationListener();
    }

    @Bean
    public UserReader userReaderD() {
        return new UserReader();
    }


    /*
        @Bean
    public FlatFileItemReader<User> userFlatFileItemReader() {
        FlatFileItemReader<User> flatFileItemReader = new FlatFileItemReader<>();
        // both PathResource and FileSystemResource work
        //  flatFileItemReader.setResource(new PathResource(pathResource));
        flatFileItemReader.setResource(new FileSystemResource(pathResource));
        flatFileItemReader.setName("CSV-reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(userLineMapper());
        return flatFileItemReader;
    }
     */

/*
    @Bean
    public LineMapper<User> userLineMapper() {
        DefaultLineMapper<User> defaultLineMapper = new DefaultLineMapper<>();
        //create a DelimitedLineTokenizer type files
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        // set header names
        lineTokenizer.setNames(new String[]{"id", "name", "dept", "salary"});
        // set each filed value
        BeanWrapperFieldSetMapper<User> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(User.class);


        // add the DelimitedLineTokenizer to the DefaultLineMapper
        defaultLineMapper.setLineTokenizer(lineTokenizer);
        // add the BeanWrapperFieldSetMapper to the DefaultLineMapper
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);
        return defaultLineMapper;
    }

 */

}
