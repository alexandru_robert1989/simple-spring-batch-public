package com.example.simplespringbatch.batch;


import com.example.simplespringbatch.model.User;
import com.example.simplespringbatch.repository.UserRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Writer used for writing the data retrieved by the reader
 * and transformed by the processor. In this exemple, the writer stores the data into
 * a H2 database.
 */
public class UserWriter implements ItemWriter<User> {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void write(List<? extends User> users) throws Exception {
        System.out.println("Data saved for Users: " + users);
        userRepository.saveAll(users);
    }
}
