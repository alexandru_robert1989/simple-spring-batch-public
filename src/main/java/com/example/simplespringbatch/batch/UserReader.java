package com.example.simplespringbatch.batch;

import com.example.simplespringbatch.model.User;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;

/**
 * Reader using the FlatFileItemReader to read and return
 * User properties from a CSV file.
 */
public class UserReader extends FlatFileItemReader<User> {

    // the csv file path
    private final String pathResource = "src/main/resources/users.csv";


    public UserReader() {
        // both PathResource and FileSystemResource work
        //  this.setResource(new PathResource(pathResource));
        this.setResource(new FileSystemResource(pathResource));
        this.setName("User-CSV-reader");
        //skip 1st line, the header
        this.setLinesToSkip(1);
        //
        this.setLineMapper(userLineMapper());
    }



    public LineMapper<User> userLineMapper() {

        // create a DefaultLineMapper of type User
        DefaultLineMapper<User> defaultLineMapper = new DefaultLineMapper<>();

        //create a DelimitedLineTokenizer type, used with csv files
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        // data is separated by , (comas)
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        // set the header/column names
        lineTokenizer.setNames(new String[]{"id", "name", "dept", "salary"});

        // set each filed value
        BeanWrapperFieldSetMapper<User> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(User.class);

        // add the DelimitedLineTokenizer to the DefaultLineMapper
        defaultLineMapper.setLineTokenizer(lineTokenizer);
        // add the BeanWrapperFieldSetMapper to the DefaultLineMapper
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);
        return defaultLineMapper;
    }


}
