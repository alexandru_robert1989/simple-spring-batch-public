package com.example.simplespringbatch.batch;

import lombok.NoArgsConstructor;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

/**
 * Listener that can be added to a {@link org.springframework.batch.core.Job}'s
 * listener property. The method after job is executed when the job finishes.
 */
@NoArgsConstructor
public class JobCompleteNotificationListener extends JobExecutionListenerSupport {


    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("The job is finished, status : " + jobExecution.getStatus());
        System.out.println("The job is finished, step execution : " + jobExecution.getStepExecutions());
        System.out.println("The job is finished, exit status : " + jobExecution.getExitStatus());
        System.out.println("The job is finished, exit status, failure exceptions : " + jobExecution.getAllFailureExceptions());

    }
}
