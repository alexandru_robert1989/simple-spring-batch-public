package com.example.simplespringbatch.batch;

import com.example.simplespringbatch.model.User;
import org.springframework.batch.item.ItemProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * The batch processor, that can be used to transform date before
 * calling the batch writer.
 * In this example we process {@link User} type data.
 */
public class UserProcessor implements ItemProcessor<User, User> {


    //map that will contain fake data
    private static final Map<String, String> DEPT_NAMES = new HashMap<>();

    //counter used for testing purposes
    public static int counter = 1;


    public UserProcessor() {
        //adding fake data
        DEPT_NAMES.put("001", "Technology");
        DEPT_NAMES.put("002", "Operations");
        DEPT_NAMES.put("003", "Accounts");
    }


    @Override
    public User process(User user) throws Exception {
        //processing uses deptCode to get the dept and set it to the user
        String deptCode = user.getDept();
        String dept = DEPT_NAMES.get(deptCode);
        user.setDept(dept);
        user.setId(counter * user.getId());
        System.out.println(String.format("Converted from [%s] to [%s]", deptCode, dept));
        counter++;
        return user;
    }
}
