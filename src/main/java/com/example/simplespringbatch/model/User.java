package com.example.simplespringbatch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {


    @Id
    private Integer id;
    private String name;
    private String dept;
    private Integer salary;
    private LocalDateTime timestamp = LocalDateTime.now();



}
